#
# Copyright 2017, Martin Owens <doctormo@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Admin site for adding repositories
"""

import os
import sys

from django.contrib.admin import ModelAdmin, site
from django.forms import ModelForm

from .models import TranslationRepository, TranslationLanguage

class RepoForm(ModelForm):
    def clean_git_url(self):
        url = self.cleaned_data['git_url']
        # XXX Check git repository is live
        return url

    def save(self, commit=True):
        obj = super(RepoForm, self).save(commit=commit)
        if obj.pk:
            obj.make_git()
        return obj


class RepoAdmin(ModelAdmin):
    list_display = ('name', 'desc', 'git_url', 'tr_dir', 'tr_find', 'target_dir')
    form = RepoForm


site.register(TranslationRepository, RepoAdmin)
site.register(TranslationLanguage)

