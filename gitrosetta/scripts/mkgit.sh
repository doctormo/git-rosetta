#!/bin/bash

TARGET=$1
REPO=$2
SUBDIR=$3

git init $TARGET
cd $TARGET
git remote add -f origin $REPO

if [ "$SUBDIR" ]; then
    git config core.sparseCheckout true
    echo $SUBDIR >> .git/info/sparse-checkout
fi

git branch --set-upstream-to=origin/master
git pull origin master

