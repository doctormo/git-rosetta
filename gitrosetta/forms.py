#
# Copyright (C) 2014 Martin Owens <doctormo@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
"""
Provide forms which allow the po file to be edited.
"""

from django.utils.functional import cached_property
from django.forms import *

class TranslationForm(Form):
    md5 = CharField(widget=HiddenInput)
    msg = CharField()
    fuzzy = BooleanField(initial=False)

    def __init__(self, pofile, **kw):
        self.pofile = pofile
        super(TranslationForm, self).__init__(**kw)

    def save(self, commit=True):
        pass
        #self.entry, Save md5 hsh here.


class TranslateForm(BaseFormSet):
    def __init__(self, lang, instance, **kw):
        self.lang = lang
        self.instance = instance
        self.pofile = self.instance.get_lang(self.lang, fl)
        super(TranslateForm, self).__init__(self, **kw)

    @cached_property
    def forms(self):
        forms = [self._construct_form(i) for i in xrange(self.total_form_count(raw=True))]
        if self.is_bound:
            forms = [ form for form in forms if form._raw_value('mechanism_id') ]
        return forms

    def save(self, commit=True):
        for field in self.entries:
            pass
        return self.instance


class UploadForm(Form):
    upload_file = FileField()

    def __init__(self, lang, instance, **kw):
        self.lang = lang
        self.instance = instance
        super(UploadForm, self).__init__(self, **kw)

    def save(self, commit=True):
        fl = self.cleaned_data('upload_file')
        self.instance.save_lang(self.lang, fl)
        return self.instance

