#
# Copyright 2017, Martin Owens <doctormo@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
We want to log the changes to translations. For credit and tracking.
"""

import os
import sys
from glob import glob
from subprocess import Popen

from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse
from django.db.models import *

from .settings import REPOSITORY_ROOT

SCRIPTS = os.path.join(os.path.dirname(__file__), 'scripts')
MKGIT = os.path.join(SCRIPTS, 'mkgit.sh')
UPGIT = os.path.join(SCRIPTS, 'upgit.sh')

class TranslationRepository(Model):
    """Each Repository that contains po files for translating"""
    name = CharField(_('Name'), max_length=64,
        help_text=_('Identifiable name used in navigation and links.'))
    slug = SlugField(_('Slug'), max_length=64)
    desc = TextField(_('Description'), null=True, blank=True)
    git_url = CharField(_('Git Https URL'), max_length=255,
        help_text=_('Publically accessable Git URL to fetch the translations'))

    tr_dir = CharField(_('PO Directory'), max_length=255, null=True, blank=True,
        help_text=_('If set, limits the Git checkout to '
            'just this directory, saving space.'))

    tr_find = CharField(_('PO Custom Finder'), max_length=255, null=True, blank=True,
        help_text=_('If set, it locates po files using this pattern '
            'if not set then po files are located automatically.'))

    class Meta:
        verbose_name_plural = _("Translation Repositories")

    @property
    def target_dir(self):
        return os.path.join(REPOSITORY_ROOT, str(self.pk))

    def make_git(self):
        """Create the required git repository"""
        script = [MKGIT, self.target_dir, self.git_url, self.tr_dir]
        return Popen(script, preexec_fn=os.setsid)

    def update_git(self):
        """Pull from the git repository remote"""
        script = [UPGIT, self.target_dir]
        return Popen(script, preexec_fn=os.setsid)

    @property
    def po_lang(self):
        """Yields a tuple of lang code and filename for each po file."""
        if not self.tr_find:
            self.generate_finder()
        for fn in self.po_files:
            fn.replace(self.target_dir, '')
            ret = re.match(self.tr_find, fn)
            if ret and 'lang' in ret.groupdict():
                yield (ret.groupdict()['lang'], fn)
            elif ret:
                raise KeyError("PO File RegEx doesn't provide 'lang'")

    @property
    def languages(self):
        """Yields a TranslationLanguage object for each language"""
        for (lang, _) in self.po_lang:
            try:
                return TranslationLanguage.objects.get(code=ret['lang'])
            except TranslationLanguage.DoesNotExist:
                pass # TranslationLanguage doesn't exist

    @property
    def po_files(self):
        """Yields all po files in the repository directory"""
        for (path, files, dirs) in os.walk(self.target_dir):
            for fl in files:
                if fl.endswith('.po'):
                    yield os.path.join(path, fl)

    def get_absolute_url(self):
        return reverse('gitr:repository', kwargs={'slug': self.slug})

    def __str__(self):
        return self.name


class TranslationLanguage(Model):
    """Each language that we can detect, know the names of and get stats for"""
    # TODO: We might need a way to match extra codes where languages have many.
    code = SlugField(max_length=12)
    name = CharField(_('English Name'), max_length=128)
    in_name = CharField(_('In Language Name'), max_length=128)

    icon = FileField(upload_to='gitr/lang/', null=True, blank=True) 
    parent = ForeignKey('self', null=True, blank=True)

    def __str__(self):
        return self.name

