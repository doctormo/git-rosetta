#
# Copyright (C) 2014 Martin Owens <doctormo@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
"""
Provide basic views for gitr site or app. It's got four types of views:

    * GitWebHook - Special DetailView call which does a git pull
    * Lists - List repositories or languages available.
    * Details - Show the Repository and it's languages
    * Updates - Upload or edit forms for one po file.

"""

from django.utils.translation import ugettext_lazy as _
from django.views.generic import ListView, UpdateView, DetailView

from .models import TranslationRepository, TranslationLanguage
from .forms import TranslateForm, UploadForm

class GitWebHook(DetailView):
    """Called by GitLab or GitHub when they get push requests."""
    template_name = 'gitr/webhook.html'
    model = TranslationRepository

    def get_context_data(self, **kw):
        ret = super(GitWebHook, self).get_context_data(**kw)
        obj = self.get_object()
        obj.update_git()
        return ret


class HomePage(ListView):
    """Show all available repositories and language links"""
    model = TranslationRepository


class AllLanguages(ListView):
    """Show all available languages and their statistics"""
    model = TranslationLanguage


class Repository(DetailView):
    """Show a repostory's languages and their statistics"""
    model = TranslationRepository


class DownloadLanguage(Repository):
    """Return the po file with the right content-type and filename"""
    def dispatch(self):
        obj = self.get_object()
        filename = dict(obj.po_lang)[self.kwargs['lang']]

        with open(filename, 'r') as fhl:
            response = HttpResponse(fhl.read())

        out = "%s-%s.po" % (obj.slug, self.kwargs['lang'])
        response['Content-Disposition'] = 'attachment; filename=%s.po' % out
        response['Content-Type'] = 'text/x-gettext-translation'
        return response


class TranslateLanguage(UpdateView):
    """Edit a po file in place, requires the repository and language"""
    model = TranslationRepository

    def get_form(self):
        return TranslateForm(self.kwargs['lang'], **self.get_form_kwargs())


class UploadLanguage(TranslateLanguage):
    """Upload a replacement po file, requires the repository and language"""
    def get_form(self):
        return UploadForm(self.kwargs['lang'], **self.get_form_kwargs())


