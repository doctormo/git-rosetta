# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf.global_settings import LANGUAGES
from django.utils.translation import activate
from django.core.files.base import File
from django.db import migrations, models

from os.path import dirname, abspath, join, isfile
FIXTURE_DIR = abspath(join(dirname(__file__), '..', 'fixtures', 'icons'))

def populate(apps, schema_editor):
    """Adds each of the django languages to the default languages list"""
    cls = apps.get_model("gitrosetta", "TranslationLanguage")
    if cls.objects.count() == 0:
        for lang, name in LANGUAGES:
            if lang in ('zh-cn', 'zh-tw'):
                continue

            # XXX Not sure why this doesn't work as expected
            activate(lang)
            in_name = str(name)
            activate('en')
            name = str(name)
            icon = None

            svg_path = join(FIXTURE_DIR, lang+'.svg')
            if isfile(svg_path):
                icon = File(file(svg_path))

            cls.objects.create(
                code=lang,
                name=name,
                in_name=in_name,
                icon=icon,
            )


def depopulate(*args):
    pass

class Migration(migrations.Migration):

    dependencies = [
        ('gitrosetta', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(populate, depopulate),
    ]
