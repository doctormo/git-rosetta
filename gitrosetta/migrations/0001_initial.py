# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='TranslationLanguage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('code', models.SlugField(max_length=12)),
                ('name', models.CharField(max_length=128, verbose_name='English Name')),
                ('in_name', models.CharField(max_length=128, verbose_name='In Language Name')),
                ('icon', models.FileField(null=True, upload_to=b'gitr/lang/', blank=True)),
                ('parent', models.ForeignKey(blank=True, to='gitrosetta.TranslationLanguage', null=True)),
            ],
        ),
        migrations.CreateModel(
            name='TranslationRepository',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(help_text='Identifiable name used in navigation and links.', max_length=64, verbose_name='Name')),
                ('slug', models.SlugField(max_length=64, verbose_name='Slug')),
                ('desc', models.TextField(null=True, verbose_name='Description', blank=True)),
                ('git_url', models.CharField(help_text='Publically accessable Git URL to fetch the translations', max_length=255, verbose_name='Git Https URL')),
                ('tr_dir', models.CharField(help_text='If set, limits the Git checkout to just this directory, saving space.', max_length=255, null=True, verbose_name='PO Directory', blank=True)),
                ('tr_find', models.CharField(help_text='If set, it locates po files using this pattern if not set then po files are located automatically.', max_length=255, null=True, verbose_name='PO Custom Finder', blank=True)),
            ],
            options={
                'verbose_name_plural': 'Translation Repositories',
            },
        ),
    ]
