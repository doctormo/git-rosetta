#
# Copyright (C) 2017 Martin Owens <doctormo@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

import os

from django.core.exceptions import ImproperlyConfigured

try:
    from django.conf import settings
except ImportError:
    settings = None

from . import __version__ as VERSION

# Number of messages to display per page.
MESSAGES_PER_PAGE = 10

# Displays this language beside the original MSGID in the admin
MAIN_LANGUAGE = None

# Change these if the source language in your PO files isn't English
MESSAGES_SOURCE_LANGUAGE_CODE = 'en'
MESSAGES_SOURCE_LANGUAGE_NAME = 'English'

CACHE_NAME = 'gitr'

# Line length of the updated PO file
POFILE_WRAP_WIDTH = 78

REPOSITORY_ROOT = None

for (name, default) in locals().items():
    locals()[name] = getattr(settings, 'GITROSETTA_'+name, default)

if not REPOSITORY_ROOT:
    raise ImproperlyConfigured('Django GIT Rosetta needs GITROSETTA_REPOSI'
        'TORY_ROOT to be set in order to function.')

if not os.path.isdir(REPOSITORY_ROOT):
    try:
        os.makedirs(REPOSITORY_ROOT)
    except IOError as err:
        raise ImproperlyConfigured('GITROSETTA_REPOSITORY_ROOT does not exist'
            ' and can not be created. (%s)' % REPOSITORY_ROOT)

