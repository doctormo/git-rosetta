#
# Copyright 2017, Martin Owens <doctormo@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
The default urls for gitr have a namespace built in and it doesn't
need to be specified in your urls.py file, only include access.
"""

from django.conf.urls import patterns, url, include

from .views import *

def url_tree(regex, *urls, **kw):
    return url(regex, include(patterns('', *urls), **kw))

urlpatterns = patterns('gitrosetta.views', url_tree('',
  url(r'^$',                  HomePage.as_view(), name='home'),
  url(r'^all/(?P<lang>[\w-]+)/$', AllLanguages.as_view(), name='language'),
  url_tree(r'^(?P<slug>[\w-]+)/',
    url(r'^$',                Repository.as_view(), name='repository'),
    url(r'^hook/$',           GitWebHook.as_view(), name='git-hook'),
    url_tree(r'^(?P<lang>.*)/',
      url(r'^$',              TranslateLanguage.as_view(), name='translate'),
      url(r'^download/$',     DownloadLanguage.as_view(), name='download'),
      url(r'^upload/$',       UploadLanguage.as_view(), name='upload'),
    ),
  ),
namespace="gitr"))
