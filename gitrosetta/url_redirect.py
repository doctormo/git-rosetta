#
# Copyright 2017, Martin Owens <doctormo@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Provide the required redirection for the default or selected language.
"""

from django.conf.urls import patterns, url, include
from django.conf import settings

from django.http import Http404
from django.shortcuts import redirect

LANGUAGES = dict(settings.LANGUAGES)

def redirect_view(request, *args, **kw):
    # XXX This just doesn't work yet and I have no idea why not.
    url = kw['url'].split('/')
    if not url or url[0] not in LANGUAGES:
        # XXX in the future this could detect the best language to use.
        return redirect('/%s/%s' % (settings.LANGUAGE_CODE, kw['url']))
    raise Http404("Can't find URL")

urlpatterns = patterns('',
  url(r'^(?P<url>.*)$', redirect_view, name="redirect")
)

